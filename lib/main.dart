import 'package:flutter/material.dart';
import 'package:flutter_app_demo1/screens/wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_app_demo1/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_demo1/models/user.dart';
import 'package:flutter_app_demo1/screens/home/homebuttons.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<TheUser>.value(
      value: AuthService().user,
      initialData: null,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Wrapper(),
      ),
    );
  }
}
