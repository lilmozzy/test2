import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Edit Profile',
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 18.0),
        children: <Widget>[
          Column(
            children: [
              SizedBox(
                height: 80,
              ),
            ],
          ),
          SizedBox(
            height: 60.0,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Username",
              labelStyle: TextStyle(fontSize: 20),
              filled: true,
            ),
          ),
          SizedBox(
            height: 60.0,
          ),
          TextField(
              decoration: InputDecoration(
            labelText: "Phone Number",
            labelStyle: TextStyle(fontSize: 20),
            filled: true,
          )),
          SizedBox(
            height: 20.0,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Full Name",
              labelStyle: TextStyle(fontSize: 20),
              filled: true,
            ),
          ),
          SizedBox(
            height: 60.0,
          ),
          TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "New Password",
                labelStyle: TextStyle(fontSize: 20),
                filled: true,
              )),
          SizedBox(
            height: 20,
          ),
          Column(
            children: <Widget>[
              ButtonTheme(
                height: 50,
                disabledColor: Colors.blueAccent,
                child: ElevatedButton(
                  onPressed: null,
                  child: Text('Save'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  child: RaisedButton(
                      child: Text('Go Back'),
                      onPressed: () {
                        Navigator.pop(context);
                      }))
            ],
          )
        ],
      ),
    ));
  }
}
