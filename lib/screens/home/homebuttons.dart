import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_app_demo1/screens/home/editprofile/editscreen (1).dart';

// YOUTUBE LINK
String _launchUrl = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';

Future<void> _launchInBrowser(String url) async {
  if (await canLaunch(url)) {
    await launch(
      url,
      forceSafariVC: false,
      forceWebView: false,
      headers: <String, String>{'header_key': 'header_value'},
    );
  } else {
    throw 'Could not launch $url';
  }
}

//Rickroll Button
class RickRollButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Center(
          child: OutlinedButton(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 9),
            child: Text('NOT RICKROLL', style: TextStyle(color: Colors.black))),
        onPressed: () {
          _launchInBrowser(_launchUrl);
        },
      ))
    ]);
  }
}

//Photobutton
class PhotoButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Center(
          child: OutlinedButton(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text('SEE PROOF', style: TextStyle(color: Colors.black))),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ProofPhoto();
          }));
        },
      ))
    ]);
  }
}

//photo
class ProofPhoto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('assets/images/defnotrickroll.jpg');
    Image image = Image(
      image: assetImage,
    );
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [0.6, 0.9],
              colors: [
                Colors.pink[100],
                Colors.white,
              ],
            ),
          ),
          child: Center(
            child: image,
          ),
        ),
      ),
    );
  }
}

class ProfileButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: RaisedButton(
            child: Text('Edit Profile'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return LoginPage();
              }));
            }));
  }
}
