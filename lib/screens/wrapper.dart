import 'package:flutter/material.dart';
import 'package:flutter_app_demo1/screens/authenticate/authenticate.dart';
import 'package:flutter_app_demo1/screens/home/home.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_demo1/models/user.dart';
import 'package:flutter_app_demo1/screens/home/homebuttons.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<TheUser>(context);

    if (user == null) {
      return Authenticate();
    } else {
      return Home();
    }
  }
}
