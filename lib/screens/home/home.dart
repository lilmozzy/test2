import 'package:flutter/material.dart';
import 'package:flutter_app_demo1/services/auth.dart';
import 'package:flutter_app_demo1/screens/home/homebuttons.dart';

class Home extends StatelessWidget {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0.6, 0.9],
          colors: [
            Colors.pink[100],
            Colors.white,
          ],
        ),
      ),
      child: Scaffold(
          backgroundColor: Color(0x00000000),
          appBar: AppBar(
            title: Text('Welcome'),
            backgroundColor: Colors.pink[200],
            elevation: 0.0,
            actions: <Widget>[
              FlatButton.icon(
                icon: Icon(Icons.person),
                label: Text('logout'),
                onPressed: () async {
                  await _auth.signOut();
                },
              ),
            ],
          ),
          body: Column(
            children: <Widget>[
              RickRollButton(),
              PhotoButton(),
              ProfileButton()
            ],
          )),
    );
  }
}
